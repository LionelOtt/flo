#include "gaussian_dataset.h"


GaussianDataset::GaussianDataset(int clusters, int dimensions, int seed)
    :   m_clusters(clusters)
      , m_dimensions(dimensions)
      , m_generator()
{
    std::random_device rng_dev;
    int seed_value = seed != 0 ? seed : rng_dev();
    m_generator.seed(seed_value);

    create_model();
}

void GaussianDataset::generate_uniform(int points)
{
    int points_per_cluster = points / m_clusters;
    for(int i=0; i<m_clusters; ++i)
    {
        auto data = sample_from_gaussian(m_mu[i], m_sigma[i], points_per_cluster);
        m_points.insert(
                m_points.end(),
                data.begin(),
                data.end()
        );

        m_ground_truth.insert(
                m_ground_truth.end(),
                points_per_cluster,
                i
        );
    }
}

void GaussianDataset::generate_random(int points)
{
    // Determine number of points to be used per cluster
    UniformDist_t weight_dist(0.1, 1.0);
    double weight_sum = 0.0;
    std::vector<double> cluster_weights;
    for(int i=0; i<m_clusters; ++i)
    {
        cluster_weights.push_back(weight_dist(m_generator));
        weight_sum += cluster_weights.back();
    }

    // Generate data points
    for(int i=0; i<m_clusters; ++i)
    {
        int point_count = points * cluster_weights[i] / weight_sum;
        auto data = sample_from_gaussian(m_mu[i], m_sigma[i], point_count);
        m_points.insert(
                m_points.end(),
                data.begin(),
                data.end()
        );
        m_ground_truth.insert(
                m_ground_truth.end(),
                point_count,
                i
        );
    }
}

void GaussianDataset::add_noise(double percent)
{
    UniformDist_t noise_dist(-50.0, 50.0);
    int count = percent * m_points.size();

    // Precompute some values
    std::vector<double> first_part;
    std::vector<Eigen::MatrixXd> inverse;
    for(auto & mat : m_sigma)
    {
        first_part.push_back(
                1.0 /
                std::sqrt(
                    std::pow(2.0 * M_PI, m_dimensions) * mat.determinant()
                )
        );
        Eigen::MatrixXd inv_mat = mat.inverse();
        inverse.emplace_back(inv_mat);
    }

    int points_added = 0;
    while(points_added < count)
    {
        // Generate a point
        Eigen::VectorXd point(m_dimensions);
        for(int i=0; i<m_dimensions; ++i)
        {
            point[i] = noise_dist(m_generator);
        }

        // Check if it is actually noise
        bool is_noise = true;
        for(size_t i=0; i<m_sigma.size(); ++i)
        {
            auto diff = point - m_mu[i];
            double tmp = (diff.transpose() * inverse[i] * diff);
            double prob = first_part[i] * std::exp(-0.5 * tmp);
            if(prob > 0.01)
            {
                is_noise = false;
                break;
            }
        }

        if(is_noise)
        {
            m_points.push_back(point);
            m_ground_truth.push_back(m_clusters);
            points_added++;
        }
    }
}

std::vector<Eigen::VectorXd> const& GaussianDataset::points() const
{
    return m_points;
}

std::vector<Eigen::VectorXd> GaussianDataset::points()
{
    return m_points;
}

std::vector<Eigen::VectorXd> GaussianDataset::centers() const
{
    return m_mu;
}

void GaussianDataset::create_model()
{
    UniformDist_t mu_dist(-50.0, 50.0);
    UniformDist_t sigma_dist(0.0, 5.0);

    m_mu.clear();
    m_sigma.clear();

    for(int i=0; i<m_clusters; ++i)
    {
        Eigen::VectorXd mu(m_dimensions);
        for(int i=0; i<m_dimensions; ++i)
        {
            mu[i] = mu_dist(m_generator);
        }
        Eigen::MatrixXd sigma = Eigen::MatrixXd::Identity(
                m_dimensions,
                m_dimensions
        ) * sigma_dist(m_generator);

        m_mu.push_back(mu);
        m_sigma.push_back(sigma);
    }
}

std::vector<Eigen::VectorXd> GaussianDataset::sample_from_gaussian(
        Eigen::VectorXd const&          mu,
        Eigen::MatrixXd const&          sigma,
        int                             count
)
{
    // Cholesky decomposition of sigma
    Eigen::MatrixXd A = sigma.llt().matrixLLT();

    // Generate random points
    std::vector<Eigen::VectorXd> points;
    for(int i=0; i<count; ++i)
    {
        Eigen::VectorXd z(m_dimensions);
        for(int i=0; i<m_dimensions; ++i)
        {
            z[i] = m_normal_dist(m_generator);
        }
        points.push_back(Eigen::VectorXd(mu + A * z));
    }

    return points;
}
