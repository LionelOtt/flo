#ifndef __GAUSSIAN_DATASET_H__
#define __GAUSSIAN_DATASET_H__


#include <cmath>
#include <numeric>

#include <Eigen/Cholesky>
#include <Eigen/LU>


/**
 * \brief Generates datasets containing Gaussian distributed data.
 */
class GaussianDataset
{
    typedef std::mt19937 Generator_t;
    typedef std::uniform_real_distribution<> UniformDist_t;
    typedef std::normal_distribution<> NormalDist_t;

    public:
        /**
         * \brief Creates a new Gaussian dataset.
         *
         * \param clusters the number of Gaussian clusters to generate
         * \param dimensions the number of dimensions of the data
         * \param seed the seed for the random number generator
         */
        GaussianDataset(int clusters, int dimensions, int seed=0);

        /**
         * \brief Generates data points distributed uniformly over the clusters.
         *
         * \param points the number of points to create
         */
        void generate_uniform(int points);

        /**
         * \brief Generates data points with variable number per cluster.
         *
         * \param points the number of points to create
         */
        void generate_random(int points);

        /**
         * \brief Adds points representing noise to the dataset.
         *
         * \parame percent the amoutn of points in percent of the full dataset
         *      to add as noise
         */
        void add_noise(double percent);

        /**
         * \brief Returns the points of this dataset.
         *
         * \return vector of points
         */
        std::vector<Eigen::VectorXd> const& points() const;

        /**
         * \brief Returns the points of this dataset.
         *
         * \return vector of points
         */
        std::vector<Eigen::VectorXd> points();

        /**
         * \brief Returns the centers, i.e. mean, of the clusters.
         *
         * \return mean locations of the clusters
         */
        std::vector<Eigen::VectorXd> centers() const;

    private:
        /**
         * \brief Creates a new random model based on the parameters.
         */
        void create_model();

        /**
         * \brief Samples a set of points from the specified gaussian.
         *
         * \param mu mean of the distribution
         * \param sigma variance of the distribution
         * \param count the number of points to draw
         */
        std::vector<Eigen::VectorXd> sample_from_gaussian(
                Eigen::VectorXd const&  mu,
                Eigen::MatrixXd const&  sigma,
                int                     count
        );

    private:
        //! The points of this dataset
        std::vector<Eigen::VectorXd>    m_points;
        //! Ground truth labels corresponding to the points
        std::vector<int>                m_ground_truth;
        //! Number of clusters
        int                             m_clusters;
        //! Dimensionality of the data
        int                             m_dimensions;

        //! Random number generator
        Generator_t                     m_generator;
        //! Distribution for normal numbers
        NormalDist_t                    m_normal_dist;
        //! Variances of the individual clusters
        std::vector<Eigen::MatrixXd>    m_sigma;
        //! Means of the individual clusters
        std::vector<Eigen::VectorXd>    m_mu;
};


#endif /* __GAUSSIAN_DATASET_H__ */
