#include <iostream>
#include <random>

#include <Eigen/Core>

#include <gaussian_dataset.h>

#include <facility_location_outliers.h>
#include <util.h>


int main(int argc, char *argv[])
{
    // Generate a simple 2D data set with noise
    auto ds = GaussianDataset{5, 2};
    ds.generate_uniform(1000);
    ds.add_noise(0.1);

    // Randomize the data points
    std::vector<Eigen::VectorXd> data = ds.points();
    std::random_device rng_dev;
    std::mt19937 gen(rng_dev());
    std::shuffle(data.begin(), data.end(), gen);

    // Create the clustering instance
    FacilityLocationOutliers flo(
            data,
            euclidean_distance,
            median_cost(data, euclidean_distance, 5.0),
            FacilityLocationOutliers::OutlierMode::Count,
            FacilityLocationOutliers::UpdateMode::Discounted
    );
    flo.set_outlier_count(100);

    // Perform the clustering
    auto iters = flo.iterate();

    // Print some of the results
    std::cout << "iterations=" << iters
              << " exemplars=" << flo.get_exemplars().size()
              << " energy=" << flo.get_energy()
              << " valid=" << flo.is_solution_valid()
              << std::endl;

    // Save clustering result to disk for visualization
    write_to_disk("/tmp/flo.dat", data, flo.get_assignments());

    return 0;
}
