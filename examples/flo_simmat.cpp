#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <boost/regex.hpp>

#include <facility_location_outliers.h>
#include <util.h>


/**
 * \brief Exception thrown when data parsing fails.
 */
class DataError : public std::exception
{
    public:
        DataError(std::string const& msg)
            :   m_message(msg)
        {}

        char const* what() const throw()
        {
            return m_message.c_str();
        }

    protected:
        std::string                     m_message;
};


/**
 * \brief Parses string representation of a matrix into an actual matrix.
 *
 * \param fname path to the matrix file to parse
 * \return matrix object
 */
Eigen::MatrixXd parse_matrix(std::string const& fmame);

/**
 * \brief Computes the cost between two points given a similarity matrix.
 *
 * \param a the first index
 * \param b the second index
 * \param simmat the similairty matrix used in the lookup
 * \param return the cost between the two points
 */
double cost_function(
        Eigen::VectorXd const&          a,
        Eigen::VectorXd const&          b,
        Eigen::MatrixXd const&          simmat
)
{
    return simmat(a[0], b[0]);
}


int main(int argc, char *argv[])
{
    namespace po = boost::program_options;
    using namespace std::placeholders;

    // +------------------------------------------------------------------------
    // | Parse command line parameters
    // +------------------------------------------------------------------------
    auto desc = po::options_description("Required options");
    desc.add_options()
        ("help", "Shows the help message")
        ("file", "Similarity matrix file")
        ("outliers", "Number of outliers")
    ;
    auto posop = po::positional_options_description();
    posop.add("file", 1);
    posop.add("outliers", 1);

    auto vm = po::variables_map();
    po::store(
            po::command_line_parser(argc, argv)
                .options(desc)
                .positional(posop)
                .run()
            , vm
    );

    po::notify(vm);
    if(vm.count("help") || !vm.count("file") || !vm.count("outliers"))
    {
        std::cout << desc << std::endl;
        return 1;
    }


    // +------------------------------------------------------------------------
    // | Actual program
    // +------------------------------------------------------------------------

    // Read similarity matrix from the file and build an index vector
    auto simmat = parse_matrix(vm["file"].as<std::string>());
    auto cost = simmat(0, 0);
    simmat.diagonal().fill(0.0);
    if(simmat.diagonal().maxCoeff() > 0)
    {
        throw DataError("The diagonal entries have to be all 0");
    }

    std::vector<Eigen::VectorXd> data;
    for(int i=0; i<simmat.rows(); ++i)
    {
        data.push_back(Eigen::VectorXd::Constant(1, i));
    }

    // Create clustering instance
    auto flo = FacilityLocationOutliers(
            data,
            std::bind(cost_function, _1, _2, simmat),
            cost,
            FacilityLocationOutliers::OutlierMode::Count,
            FacilityLocationOutliers::UpdateMode::Discounted
    );
    flo.set_outlier_count(std::stoi(vm["outliers"].as<std::string>()));
    // Perform the clustering
    auto iters = flo.iterate();

    // Print some of the results
    std::cout << "iterations=" << iters
              << " exemplars=" << flo.get_exemplars().size()
              << " energy=" << flo.get_energy()
              << " valid=" << flo.is_solution_valid()
              << std::endl;

    return 0;
}


Eigen::MatrixXd parse_matrix(std::string const& fname)
{
    std::vector<Eigen::VectorXd> data;
    std::ifstream input(fname);
    std::string line;

    // Parse the input file line by line
    while(input.good())
    {
        getline(input, line);

        boost::trim(line);
        std::vector<std::string> parts;
        boost::split(parts, line, boost::is_any_of(" "), boost::token_compress_on);

        if(parts.size() > 1)
        {
            auto values = Eigen::VectorXd(parts.size());
            for(size_t i=0; i<parts.size(); ++i)
            {
                values[i] = boost::lexical_cast<double>(parts[i]);
            }
            data.push_back(values);
        }
    }

    // Create the actual data matrix from the parsed data
    if(data.size() != static_cast<size_t>(data[0].size()))
    {
        throw DataError("Similarity matrix is not square.");
    }
    Eigen::MatrixXd distances(data.size(), data.size());
    for(size_t i=0; i<data.size(); ++i)
    {
        if(data[i].size() != data[0].size())
        {
            throw DataError("Encountered rows of differing length in line " + i);
        }
        distances.row(i) = data[i];
    }

    return distances;
}
