// Copyright (c) 2015, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <fstream>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/median.hpp>

#include <util.h>



double euclidean_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b)
{
    return (a - b).norm();
}

double angular_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b)
{
    double distance = 0.0;

    auto diff = a - b;
    for(int i=0; i<diff.size(); ++i)
    {
        double angle = M_PI - std::abs(std::abs(diff[i]) - M_PI);
        assert(angle >= 0.0 && angle <= 2*M_PI);
        distance += angle;
    }
    
    return distance;
}

void write_to_disk(
        std::string const&              fname,
        std::vector<Eigen::VectorXd> const& data,
        std::vector<int> const&         assignments
)
{
    // Write label and coordinates to disk
    std::ofstream out(fname);
    for(size_t i=0; i<assignments.size(); ++i)
    {
        out << assignments[i] << " " << data[i].transpose() << std::endl;
    }
}

double median_cost(
        std::vector<Eigen::VectorXd> const& data,
        std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)> dist_function,
        double                          scale
)
{
    boost::accumulators::accumulator_set<
        double, boost::accumulators::stats<
            boost::accumulators::tag::median(
                    boost::accumulators::with_p_square_quantile)
            > > accumulator;


    for(size_t i=0; i<data.size(); ++i)
    {
        for(size_t j=0; j<data.size(); ++j)
        {
            accumulator(dist_function(data[i], data[j]));
        }
    }

    return boost::accumulators::median(accumulator) * scale;
}

double median_cost(Eigen::MatrixXd const& distances, double scale)
{
    boost::accumulators::accumulator_set<
        double, boost::accumulators::stats<
            boost::accumulators::tag::median(
                    boost::accumulators::with_p_square_quantile)
            > > accumulator;


    for(int i=0; i<distances.rows(); ++i)
    {
        for(int j=0; j<distances.cols(); ++j)
        {
            accumulator(distances(i, j));
        }
    }

    return boost::accumulators::median(accumulator) * scale;
}
