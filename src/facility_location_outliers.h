// Copyright (c) 2015, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __FACILITY_LOCATION_OUTLIERS_H__
#define __FACILITY_LOCATION_OUTLIERS_H__


#include <functional>
#include <memory>
#include <unordered_set>

#include <Eigen/Core>
#include <Eigen/SparseCore>


/**
 * \brief Lagrangian duality based facility location with outlier detection.
 */
class FacilityLocationOutliers
{
    //! Definition of the dataset representation
    using Dataset_t = std::vector<Eigen::VectorXd>;
    //! Definition of the cost function expected by the method
    using CostFunction_t = std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)>;
    //! Definition of the sparse matrix used for solution storage
    using SparseMatrix_t = Eigen::SparseMatrix<double, Eigen::RowMajor>;


    public:
        enum class OutlierMode
        {
            Count,          //! Fixed number of outliers
            Threshold       //! Outliers making up a certain percentage of the lambda values
        };

        enum class UpdateMode
        {
            Discounted,     //! Discounted update of the existing solution
            Exact           //! Replace existing solution
        };


    public:
        /**
         * \brief Creates a new clustering instance.
         *
         * \param data the dataset to compute the solution for
         * \param cost_function the cost function used to compute the distance
         *      between two points
         * \param facility_cost the cost of creating a new facility
         * \param outlier_mode the method according to which outliers
         *      are selected
         * \param update_mode the method according to which the solution is
         *      updated
         */
        FacilityLocationOutliers(
                Dataset_t const&        data,
                CostFunction_t          cost_function,
                double                  facility_cost,
                OutlierMode             outlier_mode,
                UpdateMode              update_mode
        );

        /**
         * \brief Iterates the problem until convergence or timeout.
         *
         * \param max_iterations maximum number of iterations to run
         */
        int iterate(int max_iterations=500);

        /**
         * \brief Sets the number of outliers to find.
         *
         * This parameter is only used when OutlierMode::Count is used.
         *
         * \param count number of outliers to find
         */
        void set_outlier_count(int count);

        /**
         * \brief Sets the outlier weight threshold.
         *
         * This parameter is only used when OutlierMode::Threshold is used.
         *
         * \param threshold the weight threshold to use to determine the
         *      outliers
         */
        void set_outlier_threshold(double threshold);

        /**
         * \brief Sets the outlier selection mode.
         *
         * \param mode the mode according to which outliers are selected
         */
        void set_outlier_mode(OutlierMode mode);

        /**
         * \brief Sets the parameters of the step size function.
         *
         * The step size used has the following form:
         * \f$\theta_0 * \alpha^t\f$
         *
         * \param theta0 the value of the initial step size
         * \param alpha the base used in the time dependent exponential
         */
        void set_step_size_parameters(double theta0 = 1.0, double alpha=0.97);

        /**
         * \brief Returns the indices of the exemplars in the dataset.
         *
         * \return indices of the selected exemplars
         */
        std::vector<int> get_exemplars() const;

        /**
         * \brief Returns the indices of the outliers.
         *
         * \return indices of the selected outliers
         */
        std::vector<int> get_outliers() const;

        /**
         * \brief Returns the exemplar index each data point is assigned to.
         *
         * \return assignments of points to exemplar indices
         */
        std::vector<int> get_assignments() const;

        /**
         * \brief Returns the energy of the current solution.
         *
         * \return current solution's energy
         */
        double get_energy() const;

        /**
         * \brief Returns if the solution properly satisfies the constraints.
         *
         * The main criteria that is checked are the number of selected outliers.
         *
         * \return true if the solution is valid, false otherwise
         */
        bool is_solution_valid() const;


    private:
        /**
         * \brief Returns a gradient to improve the current solution.
         *
         * This method computes an assignment solution and derives a new
         * subgradient from this solution.
         *
         * \return subgradient vector improving the solution
         */
        Eigen::VectorXd compute_subgradient();

        /**
         * \brief Selects outliers according to the criteria set.
         */
        void select_outliers();

        /**
         * \brief Computes the number of outliers currently selected by the method.
         *
         * \return number of currently selected outliers
         */
        int outlier_count() const;


    private:
        //! Cost of creating a new cluster
        double                          m_facility_cost;
        //! Data points to be clustered
        std::vector<Eigen::VectorXd>    m_points;
        //! Cost function
        CostFunction_t                  m_cost_function;
        //! Number of outliers
        int                             m_outlier_count;
        //! Outlier weight threshold
        double                          m_outlier_threshold;
        //! Outlier selection mode
        OutlierMode                     m_outlier_mode;
        //! Solution update mode
        UpdateMode                      m_update_mode;
        //! Initial step size
        double                          m_step_size_theta_0;
        //! Base of the step size exponential
        double                          m_step_size_alpha;

        //! Subgradient storage
        Eigen::VectorXd                 m_subgradient;
        //! Outlier indicator matrix
        std::vector<bool>               m_outlier_indicator;
        //! Outlier index set
        std::unordered_set<int>         m_outlier_set;
        //! Exemplar indicator vector
        std::vector<bool>               m_exemplar_indicator;
        //! Current solution to the clustering problem
        SparseMatrix_t                  m_solution;
        //! Lambda values
        Eigen::VectorXd                 m_lambda;
};

#endif /* __FACILITY_LOCATION_OUTLIERS_H__ */
