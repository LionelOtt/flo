// Copyright (c) 2015, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __UTIL_H__
#define __UTIL_H__


#include <functional>
#include <string>
#include <vector>

#include <Eigen/Core>

#include <facility_location_outliers.h>


/**
 * \brief Computes the euclidean distance between two vectors.
 *
 * \param a first vector
 * \param b second vector
 * \return distance between the two vectors
 */
double euclidean_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b);

/**
 * \brief Computes the shortes angular distance between two vectors.
 *
 * \param a first vector
 * \param b second vector
 * \return distance between the two vectors
 */
double angular_distance(Eigen::VectorXd const& a, Eigen::VectorXd const& b);

/**
 * \brief Writes clustering results to disk for visualization.
 *
 * The format is as follows:
 * <exemplar id> <coord 1> ... <coord N>
 *
 * exemplar id is -1 if the point is an outlier.
 *
 * \param fname name of the file to store the data in
 * \param data dataset that was clustered
 * \param assignments assignment of points to exemplars
 */
void write_to_disk(
        std::string const&              fname,
        std::vector<Eigen::VectorXd> const& data,
        std::vector<int> const&         assignments
);

/**
 * \brief Returns the scaled median value of all point to point distances.
 *
 * \param data the points to compute the distances from
 * \param dist_function function used to compute the distance between two points
 * \param scale the scaling factor
 * \return scaled median point to point distance
 */
double median_cost(
        std::vector<Eigen::VectorXd> const& data,
        std::function<double (Eigen::VectorXd const&, Eigen::VectorXd const&)> dist_function,
        double                          scale
);

/**
 * \brief Returns the scaled median value of all point to point distances.
 *
 * \param distances the pointwise distances
 * \param scale the scaling factor
 * \return scaled median point to point distance
 */
double median_cost(Eigen::MatrixXd const& distances, double scale);


#endif /* __UTIL_H__ */
