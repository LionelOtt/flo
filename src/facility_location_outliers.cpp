// Copyright (c) 2015, Lionel Ott
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the <organization> nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>

#include <facility_location_outliers.h>


FacilityLocationOutliers::FacilityLocationOutliers(
        Dataset_t const&        data,
        CostFunction_t          cost_function,
        double                  facility_cost,
        OutlierMode             outlier_mode,
        UpdateMode              update_mode
)   :   m_facility_cost(facility_cost)
      , m_points(data)
      , m_cost_function(cost_function)
      , m_outlier_count(0)
      , m_outlier_threshold(0.0)
      , m_outlier_mode(outlier_mode)
      , m_update_mode(update_mode)
      , m_step_size_theta_0(1.0)
      , m_step_size_alpha(0.97)
      , m_subgradient(m_points.size())
      , m_outlier_indicator(m_points.size(), false)
      , m_exemplar_indicator(m_points.size())
      , m_solution(m_points.size(), m_points.size())
      , m_lambda(Eigen::VectorXd::Constant(m_points.size(), 1))
{}

int FacilityLocationOutliers::iterate(int max_iterations)
{
    // Iteration and termination variables
    int iter_count = 0;
    double epsilon = 10.0;
    double min_epsilon = 0.005;
    int convergence_count = 0;
    bool running = true;

    // Iterate until convergence
    while(running)
    {
        auto subgradient = compute_subgradient();
        auto theta = m_step_size_theta_0 *
            std::pow(m_step_size_alpha, iter_count);

        // Simple alpha update rule
        Eigen::VectorXd lambda_new =
            (m_lambda + (theta * subgradient))
            .cwiseMax(Eigen::VectorXd::Zero(m_lambda.size()));

        epsilon = (lambda_new - m_lambda).norm();
        std::swap(m_lambda, lambda_new);

        if(iter_count >= max_iterations)
        {
            running = false;
        }
        if(epsilon < min_epsilon)
        {
            if(is_solution_valid() || convergence_count > 50)
            {
                running = false;
            }
            else
            {
                std::cout << "Solution not yet valid" << std::endl;
            }
            convergence_count++;
        }
        iter_count++;
    }

    return iter_count;
}

void FacilityLocationOutliers::set_outlier_count(int count)
{
    m_outlier_count = count;
}

void FacilityLocationOutliers::set_outlier_threshold(double threshold)
{
    m_outlier_threshold = threshold;
}

void FacilityLocationOutliers::set_outlier_mode(OutlierMode mode)
{
    m_outlier_mode = mode;
}

void FacilityLocationOutliers::set_step_size_parameters(double theta0, double alpha)
{
    m_step_size_theta_0 = theta0;
    m_step_size_alpha = alpha;
}

std::vector<int> FacilityLocationOutliers::get_exemplars() const
{
    std::vector<int> ids;
    for(size_t i=0; i<m_exemplar_indicator.size(); ++i)
    {
        if(m_exemplar_indicator[i])
        {
            ids.push_back(i);
        }
    }
    return ids;
}

std::vector<int> FacilityLocationOutliers::get_outliers() const
{
    return std::vector<int>(m_outlier_set.begin(), m_outlier_set.end());
}

std::vector<int> FacilityLocationOutliers::get_assignments() const
{
    std::vector<int> ids;

    auto exemplars = get_exemplars();
    std::unordered_set<int> exemplar_set(exemplars.begin(), exemplars.end());

    for(int i=0; i<m_solution.outerSize(); ++i)
    {
        std::pair<int, double> max_entry = {-1, 0.0};
        if(!m_outlier_indicator[i])
        {
            for(SparseMatrix_t::InnerIterator itr(m_solution, i); itr; ++itr)
            {
                if((exemplar_set.find(itr.col()) != exemplar_set.end()) &&
                   (max_entry.second < itr.value())
                )
                {
                    max_entry = {itr.col(), itr.value()};
                }
            }
            if(max_entry.first == -1)
            {
                // FIXME: log error
                std::cerr << "Infeasible solution" << std::endl;
            }
        }
        ids.push_back(max_entry.first);
    }

    return ids;
}

double FacilityLocationOutliers::get_energy() const
{
    double total_energy = m_facility_cost * get_exemplars().size();

    auto point_assignments = get_assignments();
    for(size_t i=0; i<point_assignments.size(); ++i)
    {
        if(point_assignments[i] != -1)
        {
            total_energy += m_cost_function(
                    m_points[i],
                    m_points[point_assignments[i]]
            );
        }
    }

    return total_energy;
}


bool FacilityLocationOutliers::is_solution_valid() const
{
    // Verify validity of the result
    auto outliers = get_outliers();
    auto exemplars = get_exemplars();
    auto assignments = get_assignments();

    for(size_t i=0; i<assignments.size(); ++i)
    {
        // Check valid outlier state
        if(assignments[i] == -1)
        {
            if(std::find(outliers.begin(), outliers.end(), i) == outliers.end())
            {
                return false;
            }
        }
        // Check valid exemplar state
        else
        {
            if(std::find(exemplars.begin(), exemplars.end(), assignments[i]) == exemplars.end())
            {
                return false;
            }
        }
    }
    if(outlier_count() != m_outlier_count)
    {
        return false;
    }

    return true;
}

Eigen::VectorXd FacilityLocationOutliers::compute_subgradient()
{
    // TODO: make the distance computation smarter in the sense that we don't
    // need to compute it for far apart point pairs which we have to discover
    // during run time according to the used distance metric.

    int dim = m_points.size();
    double alpha = 0.95;
    std::vector<std::pair<int, int>> non_zero_indices;

    Eigen::VectorXd mu(dim);
    SparseMatrix_t new_solution(dim, dim);
    int nnz_per_row = (m_solution.nonZeros() / dim) * 1.5;
    new_solution.reserve(Eigen::VectorXi::Constant(dim, nnz_per_row));

    // Select outlier points
    select_outliers();

    // Compute exemplar and outlier candidates
    for(int j=0; j<dim; ++j)
    {
        mu[j] = m_facility_cost;
        for(int i=0; i<dim; ++i)
        {
            double diff = m_cost_function(m_points[i], m_points[j]) - m_lambda[i];
            if(diff < 0.0)
            {
                mu[j] += diff;
                non_zero_indices.push_back(std::make_pair(i, j));
            }
        }
    }

    for(int i=0; i<dim; ++i)
    {
        m_exemplar_indicator[i] = mu[i] < 0.0 ? true : false;
    }

    // Update the solution
    for(auto & coord : non_zero_indices)
    {
        if(m_exemplar_indicator[coord.second])
        {
            new_solution.insert(coord.first, coord.second) = 1.0;
        }
    }

    if(m_update_mode == UpdateMode::Exact)
    {
        m_solution = new_solution;
    }
    else if(m_update_mode == UpdateMode::Discounted)
    {
        m_solution = (alpha * new_solution) + (1.0 - alpha) * m_solution;
    }

    // Compute new subgradient
    Eigen::VectorXd subgradient = Eigen::VectorXd::Zero(dim);
    for(int i=0; i<dim; ++i)
    {
        int o_i = m_outlier_indicator[i] ? 1 : 0;
        subgradient[i] = m_solution.row(i).sum() - o_i;
    }
    subgradient = Eigen::VectorXd::Ones(dim) - subgradient;

    return subgradient;
}

void FacilityLocationOutliers::select_outliers()
{
    typedef std::pair<int, double> SortPair_t;

    // Create index, value pairs
    std::vector<SortPair_t> weights;
    weights.reserve(m_lambda.size());
    for(int i=0; i<m_lambda.size(); ++i)
    {
        weights.push_back({i, m_lambda[i]});
    }

    // Sort outlier candidates descending according to their lambda values
    std::sort(
            weights.begin(),
            weights.end(),
            [](SortPair_t const& a, SortPair_t const& b)
            {
                return a.second > b.second;
            }
    );

    // Select outliers according to the active policy
    m_outlier_indicator.assign(m_outlier_indicator.size(), false);
    m_outlier_set.clear();
    if(m_outlier_mode == OutlierMode::Count)
    {
        for(int i=0; i<m_outlier_count; ++i)
        {
            m_outlier_indicator[weights[i].first] = true;
            m_outlier_set.insert(weights[i].first);
        }
    }
    else if(m_outlier_mode == OutlierMode::Threshold)
    {
        double threshold = m_outlier_threshold * m_lambda.sum();
        int index = 0;
        while(threshold > 0.0)
        {
            m_outlier_indicator[weights[index].first] = true;
            m_outlier_set.insert(weights[index].first);
            threshold -= weights[index].second;
            index++;
        }
    }
}

int FacilityLocationOutliers::outlier_count() const
{
    int count = 0;

    for(int i=0; i<m_solution.outerSize(); ++i)
    {
        std::pair<int, double> max_entry = {-1, 0.0};
        if(m_outlier_indicator[i])
        {
            count++;
        }
        else
        {
            for(SparseMatrix_t::InnerIterator itr(m_solution, i); itr; ++itr)
            {
                if(max_entry.second < itr.value())
                {
                    max_entry = {itr.col(), itr.value()};
                }
            }
            if(max_entry.first == -1)
            {
                count++;
            }
        }
    }

    return count;
}
