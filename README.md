# README #

### Overview ###

This repository contains a simple implementation of facility location with outliers algorithm, introduced in the paper "On Integrated Clustering and Outlier Detection" [1].

[1] Ott, Lionel, et al. "On Integrated Clustering and Outlier Detection." Advances in Neural Information Processing Systems. 2014.

### Installation ###

The code is written in C++11 and depends on:

- CMake
- Eigen
- Boost

The code was only tested on Linux, especially Ubuntu 14.04. The code should compile on other distributions and operating systems. To compile the code follow the following steps:

```
# Create a build folder
cd source_folder
mkdir build
cd build
# Configure using cmake
ccmake ..
# Build the code
make
```

At this point you should have two executables in the `source_folder/bin` folder:

- flo_test
- flo_simmat

The first one runs a simple test on randomly drawn 2D Gaussian distributions. The output of the algorithm is saved to /tmp/flo.dat which can be visualized using the `scripts/plot_clustering.py` script which requires Python, Numpy, and Matplotlib to be available. The second program takes as input a file which contains a white space delimited similarity matrix of positive values and the number of outliers to find. The diagonal entries of the matrix should be all zeros.

The format of the data matrix is as follows.
```
x_11 x_12 ... x_1N
x_21 x_22 ... x_2N
...
x_N1 x_N2 ... x_NN
```

### Building the Documentation ###
The code is documented using Doxygen and a configuration file is provided inside the `doc` folder. Running Doxygen inside that folder will build the documentation for the code.